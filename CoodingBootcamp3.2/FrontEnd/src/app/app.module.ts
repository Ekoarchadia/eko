import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomlistComponent } from './roomlist/roomlist.component';
import { RoomListComponent } from './room-list/room-list.component';
import { CustomerBookingComponent } from './customer-booking/customer-booking.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomlistComponent,
    RoomListComponent,
    CustomerBookingComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
