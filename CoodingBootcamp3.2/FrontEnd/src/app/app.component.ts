import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  kamarList : Object[] = [
    {"id":"1", "tipe_kamar":"kamar 1", "price":20000},
    {"id":"2", "tipe_kamar":"kamar 2", "price":30000}
  ];
}
