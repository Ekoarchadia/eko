import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-customer-booking',
  templateUrl: './customer-booking.component.html',
  styleUrls: ['./customer-booking.component.css']
})
export class CustomerBookingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  add(){
    this.api.addData({
      'nama' : this.nama,
      'aalamat' : this.alamat,
      'email' : this.email,
      'phone' : this.phone

    });

    this.nama = "";
    this.alamat = "";
    this.email = "";
    this.phone = "";
  }
}
