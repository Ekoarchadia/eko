<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;

class roomController extends Controller
{
    public function getData(){
        DB::beginTransaction();
        try
        {
            $cutomer = Customer::get();
            DB::commit();
            return response()->json($customer, 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    public function addData(Request $request){
        DB::beginTransaction();

        try{
                $newCustomer = new Customer;
                $newCustomer->nama = $request->input('nama');
                $newCustomer->alamat = $request->input('alamat');
                $newCustomer->email = $request->input('email');
                $newCustomer->phone = $request->input('phone');
                $newCustomer->save();

                DB::commit();
                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }

    public function deleteData(Request $request){
        DB::beginTransaction();

        try{
                $data = Customer::find((integer)$request->input("id"));
                if(empty($data))
                {
                    return response()->json(["message"=>"Not Found"], 404);
                }

                $data->delete();
                DB::commit();

                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
             DB::rollBack();
            return response()->json(["message"=> $e->getMessage], 500);
        }
            
    }
}
