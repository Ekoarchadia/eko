<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kamar;

class roomController extends Controller
{
    public function getData(){
        DB::beginTransaction();
        try
        {
            $kamar = Kamar::get();
            DB::commit();
            return response()->json($kamar, 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    public function addData(Request $request){
        DB::beginTransaction();

        try{
                $newKamar = new Kamar;
                $newKamar->tipe_kamar = $request->input('tipe_kamar');
                $newKamar->harga_kamar = $request->input('harga_kamar');
                $newCustomer->save();

                DB::commit();
                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }

    public function deleteData(Request $request){
        DB::beginTransaction();

        try{
                $data = Kamar::find((integer)$request->input("id"));
                if(empty($data))
                {
                    return response()->json(["message"=>"Kamar Not Found"], 404);
                }

                $data->delete();
                DB::commit();

                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
             DB::rollBack();
            return response()->json(["message"=> $e->getMessage], 500);
        }
            
    }
}
